# Foe UK/EWNI action.friendsoftheearth.uk custom modules

https://bitbucket.org/foeewni/impact-stack-modules

This is a publicly-accessible copy of files from the main [FOE EWNI Impact Stack](https://bitbucket.org/foeewni/impact-stack) private repo.

`./foeuk` within this repo is a duplicate of `./drupal/web/sites/all/modules/custom/foeuk` in the main repo.

## Usage and development

This repo can be *used* independently, but can't be *developed* independently. (The `dist` files are created via a `yarn run build` from the main repository linked above.)

Changes should be made there first, including any code review steps, and finally copied over to update this repo.

Yes, I know.

## Release

Production release is ultimately done by More Onion. To get code changes over to them:

- Compile assets within the main repo
- Copy the folder over from the main repo
- Commit those changes (including the compiled `dist` directory) to the `main` branch
- Tag the commit within git with a suitably-incremented version number
- Provide a link directly to that tag in BitBucket via a support request to More Onion
