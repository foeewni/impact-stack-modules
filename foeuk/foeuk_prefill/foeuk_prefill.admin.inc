<?php

/**
 * Menu/form callback: settings form.
 */
function foeuk_prefill_settings_form($form, &$form_state) {
  $saved_settings = foeuk_prefill_settings();

  $form['#tree'] = TRUE;
  $form['settings'] = array(
    '#title' => t('General settings for the Prefill Service.'),
    '#descrition' => t('These settings are applied site-wide.'),
    '#type' => 'fieldset',
  );
  $form['settings']['endpoint'] = array(
    '#title' => t('Endpoint URL'),
    '#description' => t('Choose whether this installation should operate on production or develop environments.'),
    '#type' => 'select',
    '#options' => array(
      'https://service-prefill-dev.azurewebsites.net' => 'Development',
      'https://service-prefill-prod.azurewebsites.net' => 'Production',
    ),
    '#default_value' => $saved_settings['endpoint'],
  );
  $form['settings']['salt'] = array(
    '#title' => t('The security salt used to encode the checksum.'),
    '#type' => 'password',
    '#default_value' => '',
  );
  if ($saved_settings['salt']) {
    $form['settings']['salt_check'] = array(
      '#type' => 'markup',
      '#markup' => t('<div style="margin: -10px 0 10px 0">Salt key stored: <code>@start...@end</code></div>',
        array(
          '@start' => substr($saved_settings['salt'], 0, 3),
          '@end' => substr($saved_settings['salt'], -3),
        ),
      ),
    );
  }

  $form['settings']['timeout'] = array(
    '#title' => t('Javascript XHR request timeout in milliseconds.'),
    '#type' => 'textfield',
    '#default_value' => $saved_settings['timeout'],
  );

  $form = system_settings_form($form);
  array_unshift($form['#submit'], 'foeuk_prefill_settings_form_submit');
  return $form;
}

function foeuk_prefill_settings_form_submit($form, &$form_state) {
  $result = array(
    'endpoint' => $form_state['values']['settings']['endpoint'],
    'timeout' => (int) $form_state['values']['settings']['timeout'],
  );
  // Check if we have a new salt entered in the form, if not keep the same as before
  if ($form_state['values']['settings']['salt'] !== '') {
    $result['salt'] = $form_state['values']['settings']['salt'];
  } else {
    $saved_settings = foeuk_prefill_settings();
    $result['salt'] = $saved_settings['salt'];
  }
  variable_set('foeuk_prefill_settings', $result);
  // Don't save those into variables in system_settings_form_submit().
  unset($form_state['values']['settings']);
}
