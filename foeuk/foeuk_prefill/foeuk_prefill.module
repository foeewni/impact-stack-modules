<?php
define('__FOE_PREFILL__', drupal_get_path('module', 'foeuk_prefill'));

/**
 * Get the foeuk prefill settings.
 */
function foeuk_prefill_settings() {
  $saved_settings = variable_get('foeuk_prefill_settings', array()) + array(
    'endpoint' => 'https://service-prefill-dev.azurewebsites.net',
    'salt' => '',
    'timeout' => 5000,
  );
  return $saved_settings;
}

/**
 * Implements hook_menu();
 */
function foeuk_prefill_menu() {
  $items['admin/foeuk/prefill'] = array(
    'title' => 'FoE UK Prefill Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('foeuk_prefill_settings_form'),
    'description' => 'FoE UK Prefill settings.',
    'access arguments' => array('administer foeuk modules'),
    'file' => 'foeuk_prefill.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_preprocess_node();
 */
function foeuk_prefill_preprocess_node(&$variables) {
  $node = $variables['node'];
  $params = drupal_get_query_parameters();
  $saved_settings = foeuk_prefill_settings();
  $drupal_settings = array(
    'foeprefill' => array(
      'endpoint' => $saved_settings['endpoint'],
      'timeout' => $saved_settings['timeout'],
    ),
  );

  $nid = null;
  $urn_hash = null;
  $submission = false;

  if (isset($params['user'])) {
    drupal_add_css( __FOE_PREFILL__ . '/dist/foeuk_prefill.css');
    drupal_add_js( __FOE_PREFILL__ . '/dist/foeuk_prefill.js', array('group' => JS_DEFAULT));

    $nid = $node->nid;
    $urn_hash = $params['user'];
  }

  if ($node->type === 'thank_you_page' && isset($params['prefill'])) {
    drupal_add_js( __FOE_PREFILL__ . '/dist/foeuk_prefill.js', array('group' => JS_DEFAULT));

    $prefill_data = explode('|', base64_decode($params['prefill']));
    $nid = $prefill_data[0];
    $urn_hash = $prefill_data[1];
    $submission = true;
  }

  $check_hash = hash_hmac('sha512', $nid, $saved_settings['salt']);

  $drupal_settings['foeprefill']['data'] = array(
    'nid' => $nid,
    'chk' => $check_hash,
    'urn' => $urn_hash,
    'submission' => $submission,
  );

  drupal_add_js($drupal_settings, 'setting');
}
