/**
 * @file webform-foe-prefill-api.js
 * Pulls data either from Drupal backend or directly from our API and slams it into our form.
 */
(($, Drupal, store) => {
  // The default salutation to use if no override is provided
  // @TODO: Test entity-type of current page, and provide a suitable default (not always a petition)
  const defaultSalutation = '<span class="js-prefill-name">Friend</span>, sign the petition';

  // The optional salutation override, extracted from a hidden input within the form's first page
  let customSalutation = false;

  /**
   * Helper function to hide opt in a specific channel
   * if the main related field has been prefilled
   *
   * @constructor
   * @param {object} name - Name of the opt in.
   */

  function hideOptIn(name) {
    const $inputs = $('form').find(`[data-form-prefill-write="${name}"]`);

    $inputs.each(function forEach() {
      const $input = $(this);
      const $wrapper = $input.parents('.webform-component');
      if ($input.attr('required') === 'required') {
        const val = $input.attr('value');

        switch (val) {
          case 'opt-in':
          case '1':
          case 'yes':
          case 'HY':
          case 'true': {
            $input.prop('checked', true).trigger('change');
            $wrapper.addClass('js-prefill-hidden element-invisible');
            const $hidden = $('form').find(`[data-form-prefill-write="${name}_set_by_prefill"]`);
            $hidden.val('1');
            break;
          }
          default:
            break;
        }
      } else {
        $wrapper.addClass('js-prefill-hidden element-invisible');
      }
    });
  }

  /**
   * Removes loading spinner.
   */
  function showForm(selector) {
    setTimeout(() => {
      const $form = $(selector);
      $form.removeClass('foe_webform_prefill_get-active');
    }, 250);
  }

  /**
   * Clears the form and removes salutation.
   */
  function clearForm(selector) {
    const $form = $(selector);
    $form.attr('user', false);

    // Show any fields that were hidden and re-enable browser autocompletion
    $form.find('.js-prefill-hidden input, input.js-prefill-hidden').prop('readonly', false);
    $form.find('.js-prefill-hidden').removeClass('js-prefill-hidden element-invisible');

    const $inputs = $form.find('input');

    $inputs.each(function forEach() {
      // We are not using a quicker .parent().hasClass() here because we try to replicate
      // the module behaviour that runs when 'change' event is triggered.
      const $input = $(this);


      // Skip webform navigation/submission buttons
      if ($input.hasClass('form-submit')) {
        return;
      }

      const $triggerElement = $input.closest('.webform-component');

      // Make sure both elements and method are not undefined
      if (typeof $triggerElement !== 'undefined' && typeof $triggerElement.attr('class') !== 'undefined') {
        if ($triggerElement.attr('class').match(/webform-component--[^ ]+/)[0]) {
          if ($input.attr('type') === 'radio' || $input.attr('type') === 'checkbox') {
            $input.prop('checked', false).trigger('change').addClass('pristine');
          } else {
            $input.val('').trigger('change').addClass('pristine');
          }

          const name = $input.attr('data-form-prefill-write');
          if (name && name.indexOf('_set_by_prefill') > 0) {
            $input.val('');
          }
        }
      }

      // Unset prefill session for PHP
      // Not strictly needed after removing server-side prefill
      if ($input.attr('name') === 'prefill_session') {
        $input.val('false');
      }
    });

    $('.prefill-salutation').addClass('element-invisible');
    store.destroy(true);

    // Unset all errors
    setTimeout(() => {
      $('.field-error').removeClass('field-error');
      $('.field-success').removeClass('field-success');
    }, 50);

    // Clear all prefill sessionStorage items
    for (let i = 0; i < sessionStorage.length; i++) {
      if (/_prefill:/.test(sessionStorage.key(i))) {
        sessionStorage.removeItem(sessionStorage.key(i));
      }
    }

    // Add a sessionStorage disabling flag to prevent FOE prefill from running again in this session
    sessionStorage.setItem('foe-prefill-reset', 1);

    // Revert prefill storage to more onion's default
    $form.formPrefill({
      prefix: 'webform_prefill',
    });

    showForm();
  }

  /**
   * Appends the salutation markup at each step, using the override if present.
   * Also adds form-reset behaviour to any "not you" links (usually within the new salutation block)
   */
  function appendSalutation($form, data) {
    $form.prepend(`
      <div class="prefill-salutation element-invisible">
        <h3>${customSalutation || defaultSalutation}</h3>
        <a href="#" class="js-prefill-reset">Not <span class="js-prefill-name">you</span>?</a>
      </div>
    `);

    // Handling for the "not you" reset action
    $('.js-prefill-reset', $form)
      .on('click', (event) => {
        event.preventDefault();
        clearForm($form);

        const progressbarSelector = '.webform-progressbar';
        const activeFirstStepSelector = '.webform-progressbar > .webform-progressbar-page.current:first-child';
        if (
          document.querySelectorAll(progressbarSelector).length
          && !document.querySelectorAll(activeFirstStepSelector).length
        ) {
          // We've got a progress bar, and we're NOT on the first step of it
          // We reload the page, and fade out the form while we do so as a vague UX nicety
          const el = document.querySelector('.petition-sidebar');
          if (el) {
            el.style.transition = 'opacity 300ms ease-in-out';
            el.style.opacity = 0;
          }

          // Remove the 'user' query param
          if (window.location.search.indexOf('user') !== -1) {
            const search = window.location.search.substr(1, window.location.search.length);
            const params = search.split('&');
            let query = '';

            if (params.length > 0) {
              Object.keys(params).forEach((key) => {
                const param = params[key].split('=')[0];
                if (param === 'user') {
                  params.splice(key, 1);
                }
                query = '?'.concat(params.join('&'));
              });
            }
            window.location.href = window.location.origin.concat(window.location.pathname).concat(query);
          } else {
            window.location.reload();
          }
        }
      });
  }

  /**
   * Process the form, putting retrieved values into corresponding fields.
   * @constructor
   * @param {object} $form - The jQuery dom object.
   * @param {object} data - The Data to inject object.
   */

  function processForm($form, data) {
    // Enable prefill session
    $form.formPrefill({
      prefix: 'foe_prefill',
      map: {},
    });
    $form.addClass('form-is-being-prefilled');

    appendSalutation($form, data);

    // Hide specified elements if prefill is running (via custom CSS class 'foe-prefill-hide')
    // Also make affected inputs read-only, to disable browser auto-completion
    $form.find('.foe-prefill-hide').addClass('js-prefill-hidden element-invisible');
    $form.find('.foe-prefill-hide input, input.foe-prefill-hide').prop('readonly', true);

    const contactName = data.first_name || data.last_name;
    const $inputs = $form.find('input');

    // Work out if this form requests a full postal address
    // (In which case we won't pre-fill/hide ANY address elements because
    // we currently only have postcode and city values, and that's WEIRD)
    let fullAddressInputCount = 0;
    const fullAddressInputIdArray = [
      'submitted[postcode]',
      'submitted[address_line_1]',
      'submitted[address_line_2]',
      'submitted[city]',
    ];
    $inputs.each(function forEach() {
      fullAddressInputCount += (fullAddressInputIdArray.indexOf(this.name) > -1);
    });

    const fullAddressRequested = fullAddressInputCount === fullAddressInputIdArray.length;

    // Process each input
    $inputs.each(function forEach() {
      const $input = $(this);
      const $wrapper = $input.parents('.webform-component');
      const key = $input.attr('data-form-prefill-write');

      // Bail out if we don't have a prefill-write key
      if (typeof key !== 'string' || key.length === 0) {
        return;
      }


      // Bail out on specific blacklisted keys
      const blacklist = [
        'url', // Honeypot
        'gift_aid', // Gift aid opt-in
      ];
      if (blacklist.includes(key)) {
        return;
      }

      // Bail out on any non-whitelisted 'hidden' and 'submit' input types
      if (/hidden|submit/.test($input.attr('type'))) {
        const whitelist = [
          'contact_number',
        ];
        if (!whitelist.includes(key)) {
          return;
        }
      }


      // Bail out if explicitly and deliberately excluded via a custom
      // 'foe-webform-skip' CSS class on the input's wrapper
      if ($wrapper.hasClass('foe-prefill-skip')) {
        return;
      }

      // Bail out on all dp/opt-in radios/checkboxes
      if (key.indexOf('dp_') !== -1
        || key.indexOf('_opt_in') !== -1
        || key.indexOf('newsletter_subscription') !== -1) {
        return;
      }

      // If the current input DOESN'T have data but IS required
      // then don't prefill it ('coz we can't) but also don't hide it.
      if (!data[key] && !$input.val() && $input.attr('required') === 'required') {
        return;
      }

      // Also bail on required radio inputs with nothing selected
      if ($input.attr('required') === 'required'
        && $input.attr('type') === 'radio'
        && $form.find(`input[name="submitted[${key}]"]:checked`).length === 0) {
        return;
      }

      // Don't partially pre-fill full addresses, it's weird. (And messes with the
      // postcode lookup UX because users won't type their postcode if it's
      // pre-filled, and then the lookup function doesn't trigger as expected.)
      //
      // TODO: Prefill whole addresses...?
      // TODO: Deal with reloads better (sessionStorage data gets loaded in regardless)
      if (fullAddressRequested) {
        if (key === 'postcode' || key === 'address_line_1' || key === 'address_line_2' || key === 'city') {
          return;
        }
      }

      // If the current input DOESN'T have data and IS NOT required
      // and DOES NOT HAVE the custom 'foe-prefill-show' CSS class
      // then hide and bail out.
      if (!data[key] && !$input.attr('required') === 'required' && !$wrapper.hasClass('foe-prefill-show')) {
        $wrapper.addClass('js-prefill-hidden element-invisible')
          .find('input').prop('readonly', true);
        switch (key) {
          case 'email':
            hideOptIn('email_opt_in');
            hideOptIn('newsletter_subscription');
            break;

          case 'postcode':
            hideOptIn('post_opt_in');
            break;

          case 'phone_number':
            hideOptIn('phone_opt_in');
            break;

          default:
            break;
        }
        return;
      }

      // Now, if we have a data value (and prefill hasn't been skipped via one
      // of the rules above) then the main event happens
      if (data[key] || $input.val()) {
        // Visually hide the wrapper element and make the input read-only to disable browser auto-complete
        // (...unless instructed NOT to hide it by the custom 'foe-prefill-show' CSS class.)
        if (!$wrapper.hasClass('foe-prefill-show')) {
          $wrapper.addClass('js-prefill-hidden element-invisible');
          $input.prop('readonly', true);
        }

        // Inject the prefill data, unless there's an existing value. (Either
        // URL-fragment supplied, or from a previous form step or submission
        // within this session, and so can be assumed to be up-to-date.)
        //
        // Override and force the foe-prefill value if the wrapper has custom
        // CSS class 'foe-prefill-force'
        if (!$input.val() || $wrapper.hasClass('foe-prefill-force')) {
          $input.val(data[key]);
        }

        // Broadcast a change
        $input.trigger('change');

        // Knock-on effects of pre-filling certain keys
        switch (key) {
          case 'email':
            hideOptIn('email_opt_in');
            hideOptIn('newsletter_subscription');
            break;

          case 'postcode':
            hideOptIn('post_opt_in');
            break;

          case 'phone_number':
            hideOptIn('phone_opt_in');
            break;

          default:
            break;
        }
      }
    });

    // Greet the user
    if (contactName !== 'Friend') {
      $('.js-prefill-name').html(contactName);
    }
    $('.prefill-salutation').removeClass('element-invisible');

    const existingOptIns = [];
    const optIns = ['email_opt_in', 'newsletter_subscription', 'post_opt_in', 'phone_opt_in'];
    optIns.forEach((element) => {
      const $optIn = $('form').find(`[data-form-prefill-write="${element}"]`);
      if ($optIn.length > 0) {
        existingOptIns.push(element);
      }
    });

    const clonedOptIns = Array.from(existingOptIns);

    existingOptIns.forEach((element) => {
      const $optIn = $('form').find(`[data-form-prefill-write="${element}"]`);
      const $wrapper = $optIn.closest('.form-item');
      if ($wrapper.hasClass('js-prefill-hidden')) {
        clonedOptIns.shift();
      }
      if (clonedOptIns.length === 0) {
        $('.webform-component--consent-fields').addClass('js-prefill-hidden element-invisible');
      }
    });

    // NOTE: The $form.on() call below was reinstated when removing server-side
    //       prefill functionality.
    // TODO: Move this to the thank-you page for reliability/consistency since
    //       it often fails client-side. (Presumably by not completing before
    //       the form-success page navigation is triggered.)

    // Intercept the form submit and send the data to the Prefill
    // Note: Preventing the event and submitting after the POST
    //       it will bug out the default webform behaviour and starts
    //       to send thousands of requests per second.
    // Maybe todo: See if we can extend the webform submit behaviour
    //             (which I need to dig out the documentation)
    // $form.on('submit', submitToPrefill); // $form.off() in clearForm()

    showForm();
  }

  async function fetchData(selector) {
    const $form = $(selector);
    $form.addClass('foe_webform_prefill_get-active');

    const controller = new AbortController();
    const { endpoint, timeout } = Drupal.settings.foeprefill;

    // As soon we boot up we'll check if we have a hash on URL
    const { urn: hash, chk, nid } = Drupal.settings.foeprefill.data;

    // 5 second timeout:
    const timeoutId = setTimeout(() => controller.abort(), timeout);

    store.destroy(true);

    try {
      const response = await fetch(`${endpoint}/api/v1/form/prefill/`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: new URLSearchParams({
          hash,
          chk,
          nid,
        }),
        signal: controller.signal,
      });

      clearTimeout(timeoutId);
      const { data, status } = await response.json();

      if (typeof data !== 'undefined' && (status === 200 || status === 201)) {
        data.urn_hash = hash;
        return data;
      }
      throw new Error('No data returned from API');
    } catch (e) {
      return null;
    }
  }

  function attachToForm(selector) {
    // Bail out if we've got a reset flag. NO PREFILL FOR YOU.
    if (sessionStorage.getItem('foe-prefill-reset')) {
      return;
    }

    const $form = $(selector);

    const data = store.getData();
    // As soon we boot up we'll check if we have a hash on URL
    const { urn: hash } = Drupal.settings.foeprefill.data;

    if (hash) {
      // Exit if we're in a errored/processed form
      if ($('.alert.alert-danger').length) {
        setTimeout(clearForm, 120);
        return;
      }

      // BEGIN reinstate previous client-side-only code
      if (data !== null && data.urn_hash === hash) {
        processForm($form, data);
      }
    }
  }

  /**
   * The actual prefill behaviour, should trigger once on load/ajax load:
   */
  Drupal.behaviors.foe_webform_prefill_get = {
    attach(context, settings) {
      // Run the campaignion prefill first, to pick up existing values. (They'll
      // either be URL-fragment supplied, or from a previous form step or submission
      // within this session, and so can be assumed to be up-to-date.)
      if (Drupal.behaviors.webform_prefill && typeof Drupal.behaviors.webform_prefill.attach === 'function') {
        Drupal.behaviors.webform_prefill.attach(context, settings);
      }
    },
  };

  /**
   * The posting behaviour, should trigger once on load/ajax load:
   */
  Drupal.behaviors.foe_webform_prefill_post = {
    attach() {
      if (Drupal.settings.foeprefill.data && Drupal.settings.foeprefill.data.submission === true) {
        const { endpoint } = Drupal.settings.foeprefill;
        const { urn, nid, chk } = Drupal.settings.foeprefill.data;

        $.ajax({
          url: `${endpoint}/api/v1/form/submit/`,
          type: 'POST',
          dataType: 'json', // expected format for response
          contentType: 'application/x-www-form-urlencoded',
          data: `hash=${urn}&nid=${nid}&chk=${chk}`,
        });
      }
    },
  };

  // Handle more onion events, for multi-step forms.
  // We'll fetch the prefill data at the initial form step and then repurpose
  // the same data for subsequent steps.
  // Available events:
  // - initialFormStep
  // - changeFormStep
  // - afterFirstFormStep

  document.addEventListener('initialFormStep', async ({ detail }) => {
    if (!detail || !detail.form) {
      return;
    }

    const { selector } = detail.form;

    // Collect and store a prefill salutation override (if present) for reuse
    // (Salutation must be on the initial form step for be picked up)
    const $salutationOverride = $(selector).find('input[name="submitted[prefill_salutation_override]"]');
    if ($salutationOverride.length && $salutationOverride.val() !== '') {
      // Replace any !name placeholder(s) with suitable spans (for later data injection)
      // @TODO: Sanitize original string?
      const parsedSalutation = $salutationOverride.val().replace('!name', '<span class="js-prefill-name">Friend</span>');
      customSalutation = parsedSalutation;
    }

    const data = await fetchData(selector);
    if (data !== null) {
      store.setData(data);
      attachToForm(selector);
    }

    showForm(selector);
  });

  document.addEventListener('changeFormStep', ({ detail }) => {
    if (!detail || !detail.form) {
      return;
    }

    const { selector } = detail.form;
    attachToForm(selector);
  });
})(window.jQuery, window.Drupal, window.store);
