/**
 * Manages the local storage
 * @return {object} - Returns accessible external services.
 */
export default class Store {
  constructor(name = 'foe-prefill-user') {
    this.storeName = name;
    this.s = window.sessionStorage;
  }

  // Service functions

  /**
   * Retrieves data on the local storage and parses it as JSON
   * @return {object} - The actual store JSON parsed, or null
   */
  getData() {
    try {
      return JSON.parse(this.s.getItem(this.storeName));
    } catch (e) {
      return null;
    }
  }

  /**
   * Retrieves a string from the campaignion default webform data stored from sessionstorage
   * @return {string} - The data
   */
  getWebformTextField(name) {
    return this.s.getItem(`webform_prefill:s:${name}`);
  }

  /**
   * Retrieves a string from the campaignion default webform data stored from sessionstorage
   * @return {string} - The data
   */
  getFoeTextField(name) {
    return this.s.getItem(`foe_prefill:s:${name}`);
  }

  /**
   * Sets JSON data on the local storage
   * @constructor
   * @param {object} data - The incoming data to store.
   */
  setData(data) {
    this.s.setItem(this.storeName, JSON.stringify(this.normalizeData(data)));
  }

  /**
   * Destroys all the data the local storage
   * @constructor
   * @param {boolean} global - If this flag is passed then destroy the whole session storage, moreonion one included.
   */
  destroy(global) {
    this.s.setItem(this.storeName, '');
    if (global === true) {
      this.s.clear();
    }
  }

  /**
   * Since we have different fields in US/UK names we need to normalize those:
   * @constructor
   * @param {object} data - The incoming data to normalize.
   * @return {object} data - Returns the normalised data.
   */
  static normalizeData(data) {
    const normData = Object.create({}, data);
    normData.street_address = data.address_line_1;
    normData.telephone_number = data.phone_number || false;
    normData.dp_email = data.email_opt_in || false;
    normData.dp_postal = data.post_opt_in || false;
    normData.dp_phone = data.telephone_opt_in || false;
    normData.dp_sms = data.sms_opt_in || false;
    normData.other = data.direct_debit || 0;
    return normData;
  }

  /**
   * Get the user hash from the query string:
   * @constructor
   * @return {string|boolean} - The Hash or false.
   */
  static getUserHash() {
    if (/[?&]user=/.test(window.location.search)) {
      const qs = window.location.search.substr(1).split('&');

      for (let i = 0; i < qs.length; i++) {
        if (qs[i].indexOf('user=') !== -1) {
          return qs[i].split('=')[1];
        }
      }
    }
    return false;
  }
}
