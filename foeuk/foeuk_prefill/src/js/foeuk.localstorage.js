

/**
 * Manages the local storage
 * @return {object} - Returns accessible external services.
 */
function Store() {
  const storeName = 'foe-prefill-user';
  const s = window.sessionStorage;

  // Service functions

  /**
   * Retrieves data on the local storage and parses it as JSON
   * @return {object} - The actual store JSON parsed, or null
   */
  function getData() {
    try {
      return JSON.parse(s.getItem(storeName));
    } catch (e) {
      return null;
    }
  }

  /**
   * Retrieves a string from the campaignion default webform data stored from sessionstorage
   * @return {string} - The data
   */
  function getWebformTextField(name) {
    return s.getItem(`webform_prefill:s:${name}`);
  }

  /**
   * Retrieves a string from the campaignion default webform data stored from sessionstorage
   * @return {string} - The data
   */
  function getFoeTextField(name) {
    return s.getItem(`foe_prefill:s:${name}`);
  }

  /**
   * Since we have different fields in US/UK names we need to normalize those:
   * @constructor
   * @param {object} data - The incoming data to normalize.
   * @return {object} data - Returns the normalised data.
   */
  function normalizeData(data) {
    data.street_address = data.address_line_1;
    data.telephone_number = data.phone_number || false;
    data.dp_email = data.email_opt_in || false;
    data.dp_postal = data.post_opt_in || false;
    data.dp_phone = data.telephone_opt_in || false;
    data.dp_sms = data.sms_opt_in || false;
    data.other = data.direct_debit || 0;
    return data;
  }

  /**
   * Sets JSON data on the local storage
   * @constructor
   * @param {object} data - The incoming data to store.
   */
  function setData(data) {
    s.setItem(storeName, JSON.stringify(normalizeData(data)));
  }

  /**
   * Destroys all the data the local storage
   * @constructor
   * @param {boolean} global - If this flag is passed then destroy the whole session storage, moreonion one included.
   */
  function destroy(global) {
    s.setItem(storeName, '');
    if (global === true) {
      s.clear();
    }
  }

  /**
   * Get the user hash from the query string:
   * @constructor
   * @return {string|boolean} - The Hash or false.
   */
  function getUserHash() {
    if (/[?&]user=/.test(window.location.search)) {
      const qs = window.location.search.substr(1).split('&');

      for (let i = 0; i < qs.length; i++) {
        if (qs[i].indexOf('user=') !== -1) {
          return qs[i].split('=')[1];
        }
      }
    }
    return false;
  }

  // Return all methods
  return {
    getData,
    getWebformTextField,
    getFoeTextField,
    setData,
    destroy,
    getUserHash,
  };
}

// Init our storage manager;
window.store = new Store();
