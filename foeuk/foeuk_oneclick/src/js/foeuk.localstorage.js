/**
 * Manages the local storage
 * @return {object} - Returns accessible external services.
 */
function Store() {
  const storeName = 'foe-oneclick-user';
  const s = window.sessionStorage;

  // Service functions

  /**
   * Retrieves data on the local storage and parses it as JSON
   * @return {object} - The actual store JSON parsed, or null
   */
  function getData() {
    try {
      return JSON.parse(s.getItem(storeName));
    } catch (e) {
      return null;
    }
  }

  /**
   * Retrieves a string from the campaignion default webform data stored from sessionstorage
   * @return {string} - The data
   */
  function getWebformTextField(name) {
    return s.getItem(`webform_prefill:s:${name}`);
  }

  /**
   * Retrieves a string from the campaignion default webform data stored from sessionstorage
   * @return {string} - The data
   */
  function getFoeTextField(name) {
    return s.getItem(`foe_oneclick:s:${name}`);
  }

  /**
   * Flatten the oneclick data to single-level key:value pairs,
   * with the keys as lowercase alpha-numeric, plus underscores.
   * @param {object} data - The incoming data to normalize.
   * @return {object} data - Returns the normalised data.
   */
  function normalizeData(data) {
    const normalisedData = {
      _oneclick_data: data, // Keep the full original object available
      ...data.name, // Flatten the name
      ...data.address, // Flatten the address
    };

    // Inject the Contact's CRM GUID
    normalisedData.contact_number = data.guid;

    // Copy across a set of whitelisted fields
    [
      'email',
      'has_active_address',
      'has_demographic_info',
      'has_phone_number',
      'token',
    ].forEach((x) => {
      normalisedData[x] = data[x];
    });

    // Rename gift_aid to prevent prefilling a checkbox and potentially causing
    // a new declaration to be created. (Unsure if that would actually happen,
    // but probably better to be safe. Also fits better with other has_* keys.)
    normalisedData.has_gift_aid = data.gift_aid;

    // Extract our FOE segmentation data into a set of flattened keys
    // Just `segment_<name>_label` and `segment_<name>_value` for now.
    if (Array.isArray(data.foe_segments)) {
      data.foe_segments.forEach((segment) => {
        const name = segment.segment.toLowerCase();
        normalisedData[`segment_${name}_label`] = segment.label;
        normalisedData[`segment_${name}_value`] = segment.value;
      });
    }

    // Extract current contact consent into flattened keys
    ['email', 'phone', 'post', 'sms'].forEach((x) => {
      normalisedData[`optin_${x}`] = data.communication_preferences[`gen-${x}`];
    });

    return normalisedData;
  }

  /**
   * Sets JSON data on the local storage
   * @constructor
   * @param {object} data - The incoming data to store.
   */
  function setData(data) {
    s.setItem(storeName, JSON.stringify(normalizeData(data)));
  }

  /**
   * Destroys all the data the local storage
   * @constructor
   * @param {boolean} global - If this flag is passed then destroy the whole session storage, moreonion one included.
   */
  function destroy(global) {
    s.setItem(storeName, '');
    if (global === true) {
      s.clear();
    }
  }

  // Return all methods
  return {
    getData,
    getWebformTextField,
    getFoeTextField,
    setData,
    destroy,
  };
}

// Init our storage manager;
window.foeuk_oneclick_store = new Store();
