/**
 * @file webform-foe-oneclick-api.js
 * Pulls data either from Drupal backend or directly from our API and slams it into our form.
 */
(($, Drupal, store) => {
  const oneclickEndpoints = {
    prefill: 'Prefill/GetPrefill',
    validate: 'Prefill/ValidateToken',
    invalidate: 'Prefill/InValidateToken',
  };

  // The default salutation to use if no override is provided
  // @TODO: Test entity-type of current page, and provide a suitable default (not always a petition)
  const defaultSalutation = '<span class="js-oneclick-name">Friend</span>, sign the petition';

  // The optional salutation override, extracted from a hidden input within the form's first page
  let customSalutation = false;

  /**
   * Helper function to hide opt in a specific channel
   * if the main related field has been oneclick-ed
   *
   * @constructor
   * @param {object} name - Name of the opt in.
   */

  function hideOptIn(name) {
    const $inputs = $('form').find(`[data-form-prefill-write="${name}"]`);

    $inputs.each(function forEach() {
      const $input = $(this);
      const $wrapper = $input.parents('.webform-component');
      if ($input.attr('required') === 'required') {
        const val = $input.attr('value');

        switch (val) {
          case 'opt-in':
          case '1':
          case 'yes':
          case 'HY':
          case 'true': {
            $input.prop('checked', true).trigger('change');
            $wrapper.addClass('js-oneclick-hidden element-invisible');
            const $hidden = $('form').find(`[data-form-prefill-write="${name}_set_by_oneclick"]`);
            $hidden.val('1');
            break;
          }
          default:
            break;
        }
      } else {
        $wrapper.addClass('js-oneclick-hidden element-invisible');
      }
    });
  }

  /**
   * Removes loading spinner.
   */
  function showForm(selector) {
    setTimeout(() => {
      const $form = $(selector);
      $form.removeClass('foe_webform_oneclick_get-active');
    }, 250);
  }

  /**
   * Clears the form and removes salutation.
   */
  function clearForm(selector) {
    const $form = $(selector);
    $form.attr('token', false);

    // Show any fields that were hidden and re-enable browser autocompletion
    $form.find('.js-oneclick-hidden input, input.js-oneclick-hidden').prop('readonly', false);
    $form.find('.js-oneclick-hidden').removeClass('js-oneclick-hidden element-invisible');

    const $inputs = $form.find('input');

    $inputs.each(function forEach() {
      // We are not using a quicker .parent().hasClass() here because we try to replicate
      // the module behaviour that runs when 'change' event is triggered.
      const $input = $(this);


      // Skip webform navigation/submission buttons
      if ($input.hasClass('form-submit')) {
        return;
      }

      const $triggerElement = $input.closest('.webform-component');

      // Make sure both elements and method are not undefined
      if (typeof $triggerElement !== 'undefined' && typeof $triggerElement.attr('class') !== 'undefined') {
        if ($triggerElement.attr('class').match(/webform-component--[^ ]+/)[0]) {
          if ($input.attr('type') === 'radio' || $input.attr('type') === 'checkbox') {
            $input.prop('checked', false).trigger('change').addClass('pristine');
          } else {
            $input.val('').trigger('change').addClass('pristine');
          }

          const name = $input.attr('data-form-prefill-write');
          if (name && name.indexOf('_set_by_oneclick') > 0) {
            $input.val('');
          }
        }
      }
    });

    $('.oneclick-salutation').addClass('element-invisible');
    store.destroy(true);

    // Unset all errors
    setTimeout(() => {
      $('.field-error').removeClass('field-error');
      $('.field-success').removeClass('field-success');
    }, 50);

    // Clear all oneclick sessionStorage items
    for (let i = 0; i < sessionStorage.length; i++) {
      if (/_oneclick:/.test(sessionStorage.key(i))) {
        sessionStorage.removeItem(sessionStorage.key(i));
      }
    }

    // Add a sessionStorage disabling flag to prevent FOE oneclick from running again in this session
    sessionStorage.setItem('foe-oneclick-reset', 1);

    // Revert oneclick storage to more onion's default
    $form.formPrefill({
      prefix: 'webform_prefill',
    });

    showForm();
  }

  /**
   * Appends the salutation markup at each step, using the override if present.
   * Also adds form-reset behaviour to any "not you" links (usually within the new salutation block)
   */
  function appendSalutation($form) {
    $form.prepend(`
      <div class="oneclick-salutation element-invisible">
        <h3>${customSalutation || defaultSalutation}</h3>
        <a href="#" class="js-oneclick-reset">Not <span class="js-oneclick-name">you</span>?</a>
      </div>
    `);

    // Handling for the "not you" reset action
    $('.js-oneclick-reset', $form)
      .on('click', (event) => {
        event.preventDefault();
        clearForm($form);

        const progressbarSelector = '.webform-progressbar';
        const activeFirstStepSelector = '.webform-progressbar > .webform-progressbar-page.current:first-child';
        if (
          document.querySelectorAll(progressbarSelector).length
          && !document.querySelectorAll(activeFirstStepSelector).length
        ) {
          // We've got a progress bar, and we're NOT on the first step of it
          // We reload the page, and fade out the form while we do so as a vague UX nicety
          const el = document.querySelector('.petition-sidebar');
          if (el) {
            el.style.transition = 'opacity 300ms ease-in-out';
            el.style.opacity = 0;
          }

          // Remove the 'token' query param
          if (window.location.search.indexOf('token') !== -1) {
            const search = window.location.search.substr(1, window.location.search.length);
            const params = search.split('&');
            let query = '';

            if (params.length > 0) {
              Object.keys(params).forEach((key) => {
                const param = params[key].split('=')[0];
                if (param === 'token') {
                  params.splice(key, 1);
                }
                query = '?'.concat(params.join('&'));
              });
            }
            window.location.href = window.location.origin.concat(window.location.pathname).concat(query);
          } else {
            window.location.reload();
          }
        }
      });
  }

  /**
   * Process the form, putting retrieved values into corresponding fields.
   * @constructor
   * @param {object} $form - The jQuery dom object.
   * @param {object} data - The Data to inject object.
   */

  function processForm($form, data) {
    // Enable oneclick session
    $form.formPrefill({
      prefix: 'foe_oneclick',
      map: {},
    });
    $form.addClass('form-is-being-oneclicked');

    appendSalutation($form);

    // Hide specified elements if oneclick is running (via custom CSS class 'foe-oneclick-hide')
    // Also make affected inputs read-only, to disable browser auto-completion
    $form.find('.foe-oneclick-hide').addClass('js-oneclick-hidden element-invisible');
    $form.find('.foe-oneclick-hide input, input.foe-oneclick-hide').prop('readonly', true);

    const contactName = data.first_name || data.last_name;
    const $inputs = $form.find('input');

    // Work out if this form requests a full postal address, and whether
    // the data includes a full address.

    let fullAddressInputCount = 0;
    const fullAddressInputIdArray = [
      'submitted[address_line_1]',
      'submitted[address_line_2]',
      'submitted[city]',
      'submitted[postcode]',
    ];
    $inputs.each(function forEach() {
      fullAddressInputCount += (fullAddressInputIdArray.indexOf(this.name) > -1);
    });

    const fullAddressRequested = fullAddressInputCount === fullAddressInputIdArray.length;
    const fullAddressAvailable = Boolean(data.address_line_1 && data.city && data.postcode);

    // Process each input
    $inputs.each(function forEach() {
      const $input = $(this);
      const $wrapper = $input.parents('.webform-component');
      const key = $input.attr('data-form-prefill-write');

      // Bail out if we don't have a oneclick-write key
      if (typeof key !== 'string' || key.length === 0) {
        return;
      }

      // Bail out on specific blacklisted keys
      const blacklist = [
        'url', // Honeypot
        'gift_aid', // Gift aid opt-in
      ];
      if (blacklist.includes(key)) {
        return;
      }

      // Bail out on any non-whitelisted 'hidden' and 'submit' input types
      if (/hidden|submit/.test($input.attr('type'))) {
        const whitelist = [
          'contact_number',
        ];
        if (!whitelist.includes(key)) {
          return;
        }
      }

      // Bail out if explicitly and deliberately excluded via a custom
      // 'foe-webform-skip' CSS class on the input's wrapper
      if ($wrapper.hasClass('foe-oneclick-skip')) {
        return;
      }

      // Bail out on all dp/opt-in radios/checkboxes
      if (key.indexOf('dp_') !== -1
        || key.indexOf('_opt_in') !== -1
        || key.indexOf('newsletter_subscription') !== -1) {
        return;
      }

      // If the current input DOESN'T have data but IS required
      // then don't prefill it ('coz we can't) but also don't hide it.
      if (!data[key] && !$input.val() && $input.attr('required') === 'required') {
        return;
      }

      // Also bail on required radio inputs with nothing selected
      if (
        (
          $input.attr('required') === 'required'
          || $input.hasClass('required')
        )
        && $input.attr('type') === 'radio'
        && $form.find(`input[name="submitted[${key}]"]:checked`).length === 0) {
        return;
      }

      // Don't partially pre-fill full addresses, it's weird. (And messes with the
      // postcode lookup UX because users won't type their postcode if it's
      // pre-filled, and then the lookup function doesn't trigger as expected.)
      if (fullAddressRequested && !fullAddressAvailable) {
        if (key === 'postcode' || key === 'address_line_1' || key === 'address_line_2' || key === 'city') {
          return;
        }
      }

      // If the current input DOESN'T have data and IS NOT required
      // and DOES NOT HAVE the custom 'foe-oneclick-show' CSS class
      // then hide and bail out.
      if (
        !data[key]
        && !(
          $input.attr('required') === 'required'
          || $input.hasClass('required')
        )
        && !$wrapper.hasClass('foe-oneclick-show')
      ) {
        $wrapper.addClass('js-oneclick-hidden element-invisible')
          .find('input').prop('readonly', true);
        switch (key) {
          case 'email':
            hideOptIn('email_opt_in');
            hideOptIn('newsletter_subscription');
            break;

          case 'postcode':
            hideOptIn('post_opt_in');
            break;

          case 'phone_number':
            hideOptIn('phone_opt_in');
            break;

          default:
            break;
        }
        return;
      }

      // Now, if we have a data value (and oneclick hasn't been skipped via one
      // of the rules above) then the main event happens
      if (data[key] || $input.val()) {
        // Visually hide the wrapper element and make the input read-only to disable browser auto-complete
        // (...unless instructed NOT to hide it by the custom 'foe-oneclick-show' CSS class.)
        if (!$wrapper.hasClass('foe-oneclick-show')) {
          $wrapper.addClass('js-oneclick-hidden element-invisible');
          $input.prop('readonly', true);
        }

        // Inject the oneclick data, unless there's an existing value. (Either
        // URL-fragment supplied, or from a previous form step or submission
        // within this session, and so can be assumed to be up-to-date.)
        //
        // Override and force the foe-oneclick value if the wrapper has custom
        // CSS class 'foe-oneclick-force'
        if (!$input.val() || $wrapper.hasClass('foe-oneclick-force')) {
          $input.val(data[key]);
        }

        // Broadcast a change
        $input.trigger('change');

        // Knock-on effects of pre-filling certain keys
        switch (key) {
          case 'email':
            hideOptIn('email_opt_in');
            hideOptIn('newsletter_subscription');
            break;

          case 'postcode':
            hideOptIn('post_opt_in');
            break;

          case 'phone_number':
            hideOptIn('phone_opt_in');
            break;

          default:
            break;
        }
      }
    });

    // Greet the user
    if (contactName !== 'Friend') {
      $('.js-oneclick-name').html(contactName);
    }
    $('.oneclick-salutation').removeClass('element-invisible');

    // If all consent inputs are hidden, hide the consent wrapper
    const existingOptIns = [];
    const optIns = ['email_opt_in', 'newsletter_subscription', 'post_opt_in', 'phone_opt_in'];
    optIns.forEach((element) => {
      const $optIn = $('form').find(`[data-form-prefill-write="${element}"]`);
      if ($optIn.length > 0) {
        existingOptIns.push(element);
      }
    });
    const clonedOptIns = Array.from(existingOptIns);
    existingOptIns.forEach((element) => {
      const $optIn = $('form').find(`[data-form-prefill-write="${element}"]`);
      const $wrapper = $optIn.closest('.form-item');
      if ($wrapper.hasClass('js-oneclick-hidden')) {
        clonedOptIns.shift();
      }
      if (clonedOptIns.length === 0) {
        $('.webform-component--consent-fields').addClass('js-oneclick-hidden element-invisible');
      }
    });

    showForm();
  }

  // Adapted from PHP function within crm_helper_functions
  function checkTokenFormatAndTimestamp(token, requestedAccessTypes = []) {
    // Normalise accessTypes input
    const requestedAccessTypesArray = typeof requestedAccessTypes === 'string' ? requestedAccessTypes.split(',') : requestedAccessTypes;

    // Split our token into the chucks we need
    const tokenParts = token.split('|');

    // Fail if we've got an unexpected number of parts
    if (tokenParts.length !== 5) {
      return false;
    }

    // Fail if we've got an expired timestamp
    const now = new Date();
    const expiry = new Date(tokenParts[2]);
    if (expiry < now) {
      return false;
    }

    // Fail if we're checking for a specific access and we don't have it
    // Only consider the first character, since the optional second 'L' is irrelevant
    if (requestedAccessTypesArray.length) {
      const tokenAccessTypes = tokenParts[3].split(',').map(x => x[0]);
      for (let i = 0; i < requestedAccessTypesArray.length; i++) {
        const accessType = requestedAccessTypesArray[i][0];
        if (tokenAccessTypes.indexOf(accessType) === -1) {
          return false;
        }
      }
    }

    // Otherwise we're okay, as far as we can tell here
    return true;
  }

  async function fetchData(selector) {
    // Collect the token, if there is one
    const params = new URL(document.location.toString()).searchParams;
    const token = params.get('token');

    // If we don't have a token, we don't need to do anything
    if (!token) {
      return null;
    }

    // Strip it from the URL to help prevent accidental sharing
    // XXXXX disabled for debug
    // params.delete('token');
    let newUrl = `${window.location.protocol}//${window.location.host}${window.location.pathname}`;
    if (params.toString()) {
      newUrl += `?${params.toString()}`;
    }
    window.history.replaceState(null, null, newUrl);

    // Check if we have a valid token query parameter
    if (!checkTokenFormatAndTimestamp(token, 'P')) {
      return null;
    }

    // Collect our form element and mark it as active ("currently being prefilled")
    const $form = $(selector);
    $form.addClass('foe_webform_oneclick_get-active');

    // Collect our config
    // eslint-disable-next-line camelcase
    const { endpoint, timeout, prefill_key: code } = Drupal.settings.foeuk_oneclick;

    // Set up our timeout
    const controller = new AbortController();
    const timeoutId = setTimeout(() => { controller.abort(); }, timeout);

    store.destroy(true);

    try {
      const prefillUrl = `${endpoint}${oneclickEndpoints.prefill}?code=${code}&Token=${token}`;
      const response = await fetch(prefillUrl, {
        method: 'GET',
        headers: { accept: 'application/json' },
        signal: controller.signal,
      });

      clearTimeout(timeoutId);
      const data = await response.json();
      const { status } = response;

      if (typeof data !== 'undefined' && (status === 200)) {
        data.token = token;
        return data;
      }
      throw new Error('No data returned from API');
    } catch (e) {
      return null;
    }
  }

  function attachToForm(selector) {
    // Bail out if we've got a reset flag. NO oneclick FOR YOU.
    if (sessionStorage.getItem('foe-oneclick-reset')) {
      return;
    }

    const $form = $(selector);

    const data = store.getData();

    if (data !== null) {
      processForm($form, data);
    }
  }

  async function invalidateToken(token) {
    // Can't do anything if we don't have a token
    if (!token) return;

    // Exit if we have a long-lived ("PL") prefill token.
    // (If it's "PL" we shouldn't expire it after using it for "P")
    const tokenPurposes = token.split('|')[3].split(',');
    if (tokenPurposes.indexOf('PL') !== -1) return;

    // Do the invalidation
    const { endpoint, invalidate_key: code } = Drupal.settings.foeuk_oneclick;
    const invalidateUrl = `${endpoint}${oneclickEndpoints.invalidate}?code=${code}&Token=${token}`;

    try {
      const response = await fetch(invalidateUrl, {
        method: 'GET',
        headers: { accept: 'application/json' },
      });

      if (response.status !== 200) {
        throw new Error(
          `The Function App endpoint "${oneclickEndpoints.invalidate}" returned a ${response.status} status code`
        );
      }
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * The actual oneclick behaviour, should trigger once on load/ajax load:
   */
  Drupal.behaviors.foe_webform_oneclick_main = {
    attach(context, settings) {
      // Run the Campaignion oneclick first, to pick up existing values. (They'll
      // either be URL-fragment supplied, or from a previous form step or submission
      // within this session, and so can be assumed to be up-to-date.)
      if (Drupal.behaviors.webform_prefill && typeof Drupal.behaviors.webform_prefill.attach === 'function') {
        Drupal.behaviors.webform_prefill.attach(context, settings);
      }

      // Invalidate token on thank you pages
      if (context === document && document.body.classList.contains('node-type-thank-you-page')) {
        const { token } = store.getData();
        invalidateToken(token);
      }
    },
  };


  // Handle more onion events, for multi-step forms.
  // We'll fetch the oneclick data at the initial form step and then repurpose
  // the same data for subsequent steps.
  // Available events:
  // - initialFormStep
  // - changeFormStep
  // - afterFirstFormStep

  document.addEventListener('initialFormStep', async ({ detail }) => {
    if (!detail || !detail.form) {
      return;
    }

    const { selector } = detail.form;

    // Collect and store a oneclick salutation override (if present) for reuse
    // (Salutation must be on the initial form step for be picked up)
    const $salutationOverride = $(selector).find('input[name="submitted[oneclick_salutation_override]"]');
    if ($salutationOverride.length && $salutationOverride.val() !== '') {
      // Replace any !name placeholder(s) with suitable spans (for later data injection)
      // @TODO: Sanitize original string?
      const parsedSalutation = $salutationOverride.val().replace('!name', '<span class="js-oneclick-name">Friend</span>');
      customSalutation = parsedSalutation;
    }

    const data = await fetchData(selector);
    if (data !== null) {
      store.setData(data);
      attachToForm(selector);
    }

    showForm(selector);
  });

  document.addEventListener('changeFormStep', ({ detail }) => {
    if (!detail || !detail.form) {
      return;
    }

    const { selector } = detail.form;
    attachToForm(selector);
  });
})(window.jQuery, window.Drupal, window.foeuk_oneclick_store);
