<?php

/**
 * Menu/form callback: settings form.
 */
function foeuk_oneclick_settings_form($form, &$form_state) {
  $saved_settings = foeuk_oneclick_settings();

  $form['#tree'] = TRUE;
  $form['settings'] = array(
    '#title' => t('Settings for the Oneclick service.'),
    '#descrition' => t('These settings are applied site-wide.'),
    '#type' => 'fieldset',
  );
  $form['settings']['endpoint'] = array(
    '#title' => t('Oneclick API base URL'),
    '#description' => t('The base URL of the function app API. Should be in the form https://domain.com/api/'),
    '#type' => 'textfield',
    '#default_value' => $saved_settings['endpoint'],
  );
  $form['settings']['prefill_key'] = array(
    '#title' => t('Prefill code'),
    '#description' => t('The function key code for the <code>Prefill/GetPrefill</code> endpoint'),
    '#type' => 'textfield',
    '#default_value' => $saved_settings['prefill_key'],
  );
  $form['settings']['validate_key'] = array(
    '#title' => t('Validate code'),
    '#description' => t('The function key code for the <code>Prefill/ValidateToken</code> endpoint'),
    '#type' => 'textfield',
    '#default_value' => $saved_settings['validate_key'],
  );
  $form['settings']['invalidate_key'] = array(
    '#title' => t('Invalidate code'),
    '#description' => t('The function key code for the <code>Prefill/InValidateToken</code> endpoint'),
    '#type' => 'textfield',
    '#default_value' => $saved_settings['invalidate_key'],
  );
  $form['settings']['timeout'] = array(
    '#title' => t('Javascript fetch request timeout in milliseconds.'),
    '#type' => 'textfield',
    '#default_value' => $saved_settings['timeout'],
  );

  $form = system_settings_form($form);
  array_unshift($form['#submit'], 'foeuk_oneclick_settings_form_submit');
  return $form;
}

function foeuk_oneclick_settings_form_submit($form, &$form_state) {
  $result = array(
    'endpoint' => $form_state['values']['settings']['endpoint'],
    'prefill_key' => $form_state['values']['settings']['prefill_key'],
    'validate_key' => $form_state['values']['settings']['validate_key'],
    'invalidate_key' => $form_state['values']['settings']['invalidate_key'],
    'timeout' => (int) $form_state['values']['settings']['timeout'],
  );
  variable_set('foeuk_oneclick_settings', $result);

  // Don't save those into variables in system_settings_form_submit().
  unset($form_state['values']['settings']);
}
