# Foe UK/EWNI action.friendsoftheearth.uk custom modules

Please keep all submodules in this folder, each subfolder needs its own `.info` and `.module` files.

To release and package up the modules for review for moreonion follow the instructions on the [public foeewni/impact-stack-modules repo](https://bitbucket.org/foeewni/impact-stack-modules).
